#!/usr/bin/env python

import argparse
import requests
import psycopg2
from psycopg2.extras import RealDictCursor

def create_subscription(project_id, topic_name, subscription_name):
    from google.cloud import pubsub_v1

    subscriber = pubsub_v1.SubscriberClient()
    topic_path = subscriber.topic_path(project_id, topic_name)
    subscription_path = subscriber.subscription_path(
        project_id, subscription_name)

    subscription = subscriber.create_subscription(
        subscription_path, topic_path)

    print('Subscription created: {}'.format(subscription))

def add_msg(message,idusuario,horario):
    con = psycopg2.connect(host='tad', database='desafio',
    user='postgres')
    cur = con.cursor()
    sql = "INSERT INTO mensagens(texto,idUsuario,horario_envio) VALUES ('%s',%s,'%s')"%(message,idusuario,horario)  
    cur.execute(sql)
    con.commit()
    con.close()
    return message

def receive_messages(project_id, subscription_name):
    import time

    from google.cloud import pubsub_v1

    subscriber = pubsub_v1.SubscriberClient()

    subscription_path = subscriber.subscription_path(
        project_id, subscription_name)

    def callback(message):
        print(message.data,message.attributes.get('user'))
        msg = add_msg(bytes.decode(message.data),message.attributes.get('user'),message.attributes.get('horario'))
        print('Mensagem enviada:'+msg)
        message.ack()

    subscriber.subscribe(subscription_path, callback=callback)

    print('Listening for messages on {}'.format(subscription_path))
    while True:
        time.sleep(60)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter
    )
    parser.add_argument('project_id', help='Your Google Cloud project ID')

    subparsers = parser.add_subparsers(dest='command')

    create_parser = subparsers.add_parser(
        'create', help=create_subscription.__doc__)
    create_parser.add_argument('topic_name')
    create_parser.add_argument('subscription_name')

    receive_parser = subparsers.add_parser(
        'receive', help=receive_messages.__doc__)
    receive_parser.add_argument('subscription_name')

    args = parser.parse_args()

    if args.command == 'create':
        create_subscription(
            args.project_id, args.topic_name, args.subscription_name)
    elif args.command == 'receive':
        receive_messages(args.project_id, args.subscription_name)