#!/usr/bin/env python

import argparse
import datetime

def create_topic(project_id, topic_name):
    from google.cloud import pubsub_v1

    publisher = pubsub_v1.PublisherClient()
    topic_path = publisher.topic_path(project_id, topic_name)

    topic = publisher.create_topic(topic_path)

    print('Topic created: {}'.format(topic))

def publish_messages(project_id, topic_name, message):
    from google.cloud import pubsub_v1

    publisher = pubsub_v1.PublisherClient()

    topic_path = publisher.topic_path(project_id, topic_name)

    data = message

    data = data.encode('utf-8')

    future = publisher.publish(topic_path, data=data)
    print(future.result())
    print('Published messages.')

    with open('/home/husky/Desktop/desafio-prototipo/cron-publish.log','a') as outFile:
        outFile.write('\nPublish: ' + str(datetime.datetime.now()))

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter
    )
    parser.add_argument('project_id', help='Your Google Cloud project ID')

    subparsers = parser.add_subparsers(dest='command')

    create_parser = subparsers.add_parser('create', help=create_topic.__doc__)
    create_parser.add_argument('topic_name')

    publish_parser = subparsers.add_parser(
        'publish', help=publish_messages.__doc__)
    publish_parser.add_argument('topic_name')
    publish_parser.add_argument('message')

    args = parser.parse_args()

    if args.command == 'create':
        create_topic(args.project_id, args.topic_name)
    elif args.command == 'publish':
        publish_messages(args.project_id, args.topic_name, args.message)