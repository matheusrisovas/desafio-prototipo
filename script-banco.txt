-- ALTER TABLE mensagens add foreign key(id_usuario) references usuarios(id);
-- ALTER TABLE usuarios add senha VARCHAR(255);
-- CREATE TABLE usuarios
--   (idUsuario serial NOT NULL, nome VARCHAR(50),senha VARCHAR(255), PRIMARY KEY(idUsuario));
-- 
-- CREATE TABLE mensagens 
--     (idMensagem serial NOT NULL, texto TEXT NOT NULL, idUsuario INTEGER, PRIMARY KEY (idMensagem), FOREIGN KEY(idUsuario) REFERENCES usuarios(idUsuario));

-- DROP TABLE mensagens, usuarios;

-- ALTER TABLE mensagens ADD horario_envio timestamp;
-- ALTER TABLE mensagens drop horario_envio ;

-- INSERT INTO usuarios VALUES (1,'Husky');
INSERT INTO mensagens(texto,idusuario,horario_envio) VALUES ('Oi mano',1,now());
-- DELETE FROM mensagens;
-- DELETE FROM usuarios;

-- SELECT idmensagem, texto, idusuario, to_char(horario_envio,'HH24:MI') as horario from mensagens;
-- SELECT m.idmensagem,m.texto,m.idusuario,to_char(m.horario_envio,'DD-MM-YYYY HH24:MI:SS') as horario,nome FROM mensagens as m,usuarios as u WHERE m.idusuario = u.idusuario;
-- UPDATE mensagens SET horario_envio = '2019-07-28 20:03';
