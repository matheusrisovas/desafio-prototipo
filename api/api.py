from flask import Flask, jsonify, request
from flask_cors import CORS, cross_origin
import psycopg2
from psycopg2.extras import RealDictCursor
import json
from datetime import datetime
import pytz
import argparse

app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

@app.route('/add_msg',methods=['POST'])
@cross_origin()
def add_msg():
    from google.cloud import pubsub_v1

    publisher = pubsub_v1.PublisherClient()

    topic_path = publisher.topic_path('desafio-123', 'topico')

    data = request.get_json()
    message = data['mensagem']
    message = message.encode('utf-8')

    idusuario = data['idusuario']
    idusuario = str(idusuario).encode('utf-8')

    currentDT= datetime.now() 
    horario = str('%s-%s-%s %s:%s'%(currentDT.year,currentDT.month,currentDT.day,currentDT.hour,currentDT.minute)).encode('utf-8')
    print(horario)
    future = publisher.publish(topic_path, data=message, user=idusuario, horario=horario)
    print(future.result())
    print("Published messages: %s"%(bytes.decode(message)))

    # with open('/home/husky/Desktop/desafio-prototipo/cron-publish.log','a') as outFile:
        # outFile.write('\nPublish: ' + str(datetime.datetime.now()))

    return jsonify(data), 201

@app.route('/add_usuario',methods=['POST'])
@cross_origin()
def add_usuario():
    data = request.get_json()
    user_nick = data['nick']
    user_senha = data['senha']
    print(user_nick,user_senha)
    con = psycopg2.connect(host='tad', database='desafio',
    user='postgres')
    cur = con.cursor(cursor_factory=RealDictCursor)
    sql = "INSERT INTO usuarios (nome,senha) VALUES ('%s','%s')"%(user_nick,user_senha)  
    cur.execute(sql)
    con.commit()
    con.close()
    return 'Sucesso',201

@app.route('/get_msg',methods=['GET'])
@cross_origin()
def get_msg():
    con = psycopg2.connect(host='tad', database='desafio',
    user='postgres')
    cur = con.cursor(cursor_factory=RealDictCursor)
    sql = "SELECT m.idmensagem,m.texto,m.idusuario,to_char(m.horario_envio,'YYYY-MM-DD HH24:MI:SS') as horario,nome FROM mensagens as m,usuarios as u WHERE m.idusuario = u.idusuario;"  
    cur.execute(sql)
    # recset = cur.fetchall()
    # con.close()
    return json.dumps(cur.fetchall()), 200

@app.route('/get_usuario',methods=['POST'])
@cross_origin()
def get_usuario():
    data = request.get_json()
    user_nick = data['nick']
    user_senha = data['senha']
    con = psycopg2.connect(host='tad', database='desafio',
    user='postgres')
    cur = con.cursor(cursor_factory=RealDictCursor)
    sql = "SELECT * FROM usuarios WHERE nome = '%s' and senha = '%s'"%(user_nick,user_senha)  
    cur.execute(sql)
    return json.dumps(cur.fetchone()),200

@app.route('/get_msg/<int:id>',methods=['GET'])
@cross_origin()
def get_msg_by_id(id):
    con = psycopg2.connect(host='tad', database='desafio',
    user='postgres')
    cur = con.cursor(cursor_factory=RealDictCursor)
    sql = f"SELECT * FROM mensagens WHERE idmensagem={id}"  
    cur.execute(sql)
    # recset = cur.fetchall()
    # con.close()
    return json.dumps(cur.fetchall()), 200

if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0') 