import "./App.css";
import { get_msg } from "./components/ChatFunctions";
import { BrowserRouter as Router, Route } from "react-router-dom";
import React, { Component } from "react";

import Chat from "./components/Chat";
import Login from "./components/Login";
import Cadastro from "./components/Cadastro";
import {ProtectedRoute} from './components/ProtectedRoute';
import NavBar from './components/NavBar';

export default class App extends Component {
  constructor() {
    super();
    this.state = {
      mensagem: "",
      mensagens: [],
      vazio: false
    };
  }
  componentDidMount() {
    setInterval(this.getMensagem, 5000);
  }

  getMensagem = () => {
    get_msg().then(res => {
      this.setState({ mensagens: res.data });
      if (this.state.mensagens.length === 0) {
        this.setState({ vazio: true });
      } else {
        this.setState({ vazio: false });
      }
    });
  };
  render() {
    return (
      <Router>
      <div className="App">
          <NavBar />
          <ProtectedRoute exact path='/' mensagens={this.state.mensagens} vazio={this.state.vazio} component={Chat} />
          <Route exact path='/login' component={Login} />
          <Route exact path='/cadastro' component={Cadastro} />
      </div>
      </Router>
    );
  }
}
