import React, { Component } from "react";
export default class ChatItem extends Component {
  horario = this.props.mensagem.horario;
  horas = (this.horario).split(':'); 
  horarioUnix = new Date(this.props.mensagem.horario);
  horarioFuso = (this.horarioUnix.setMinutes((this.horarioUnix.getMinutes())+(this.horarioUnix.getTimezoneOffset()*-1)));

  render() {
    return (
      <div>
        <li
          className={
            this.props.idusuario === this.props.mensagem.idusuario
              ? "w3-animate-zoom float-right tri-right right-top" 
              : "w3-animate-zoom float-left tri-right left-top"
          }
        >   <div className="mensagem">
            <div className="mensagem-nome">{this.props.mensagem.nome}</div>
            <div className="mensagem-texto">{this.props.mensagem.texto}</div>
            <div className="mensagem-horario">{this.horarioUnix.toLocaleTimeString('pt-BR',{hour12:false,hour:'2-digit',minute:'2-digit'})}</div>
            </div>
        </li>
      </div>
    );
  }
}
