import React, { Component } from "react";
import ChatItem from './ChatItem';
import AddMsg from './AddMsg';

export default class Chat extends Component {

  constructor(props){
    super(props);
    this.myRef = React.createRef();
  }
  scrollToBottom = () => {
    this.el.scrollIntoView({behavior: 'smooth'});
  }
  componentDidMount(){
    setTimeout(this.scrollToBottom,6000);
    // this.scrollToBottom();
  }
  render() {
    return (
      <div id='container' className={this.props.vazio ? 'container bg-no-gradient' : 'container bg-gradient'}>
        <div className="content">
        <div className={this.props.vazio ? 'center' : 'none'}>
        <img src={require("../images/figure-welcome.png")} alt='Welcome' />
          <h1>Bem vindo ao chat da KeyCar</h1>
          <p>
            Digite uma mensagem no campo abaixo para iniciar uma conversa em
            grupo
          </p>
          </div>
          <ul id='chat-list' className={this.props.vazio? 'chat-list style-7 none' : 'chat-list style-7 visible'}>
            {this.props.mensagens.map(mensagem => (
              <ChatItem key={mensagem.idmensagem} idusuario={JSON.parse(localStorage.getItem('desafio-keycar')).idusuario} 
              mensagem={mensagem}/>
            ))}
            <div ref={el => { this.el = el; }}></div>
          </ul>
        </div>
              <AddMsg vazio={this.props.vazio}/>
      </div>
    );
  }
}
