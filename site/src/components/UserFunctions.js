import axios from "axios";

export const registrarUsuario = novoUsuario => {
  console.log(novoUsuario);
  return axios
    .post("http://0.0.0.0:5000/add_usuario", {
      nick: novoUsuario.nick,
      senha: novoUsuario.senha
    })
    .then(res => {
      console.log(res);
      return res;
    });
};

export const login = usuario => {
  return axios
    .post("http://0.0.0.0:5000/get_usuario", {
      nick: usuario.nick,
      senha: usuario.senha
    })
    .then(res => {
      if (res.data) {
        console.log(res.data);
        localStorage.setItem("desafio-keycar", JSON.stringify(res.data));
        return res.data;
      } else {
        return null;
      }
    })
    .catch(err => {
      console.log(err);
    });
};
