import React, { Component } from "react";
import { withRouter } from 'react-router-dom';
import Swal from 'sweetalert2';

import { enviarMensagem } from "./ChatFunctions";

class AddMsg extends Component {
  constructor() {
    super();
    this.state = {
      mensagem: ""
    };
  }
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
    console.log(`Mensagem: ${this.state.mensagem}`);
  };
  onSubmit = e => {
    e.preventDefault();
    const novaMensagem = {
      mensagem: this.state.mensagem,
      idusuario: JSON.parse(localStorage.getItem('desafio-keycar')).idusuario
    };
    enviarMensagem(novaMensagem);
    this.setState({ mensagem: "" });
  };
  onLogout = ()=>{
    localStorage.removeItem('desafio-keycar');
    Swal.fire({
      title: "Sucesso!",
      text: "Logout realizado com sucesso!",
      type: "success",
      button: "Ok!"
    }).then(() => {
      this.props.history.push(`/login`);
    });
  }
  render() {
    return (
        <form onSubmit={this.onSubmit} className="form">
          <img src={require("../images/logo-keycar.png")} alt='LogoKeyCar' className="logo pointer" onClick={this.onLogout} />
          <input
            className="input-text"
            type="text"
            name="mensagem"
            onChange={this.onChange}
            value={this.state.mensagem}
            placeholder="Digite sua mensagem..."
          />
          <input
            alt='Seta'
            type="image"
            src={require("../images/button-send-normal.svg")}
            className={this.props.vazio?"input-submit":'input-submit'}
          />
        </form>
    );
  }
}

export default withRouter(AddMsg);