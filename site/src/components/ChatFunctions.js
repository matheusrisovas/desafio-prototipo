import axios from 'axios';

export const enviarMensagem = novaMensagem => {
    console.log(novaMensagem);
    return axios
        .post('http://0.0.0.0:5000/add_msg', {
            mensagem:novaMensagem.mensagem,
            idusuario: novaMensagem.idusuario
        })
        .then(res => {
            console.log(res);
            return res;
        })
}

export const get_msg = () => {
    return axios
    .get('http://0.0.0.0:5000/get_msg', 
    (req, res) => {
        console.log(res);
        
        res.send(res);
    })
}