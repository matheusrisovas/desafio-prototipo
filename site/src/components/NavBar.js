import React, { Component } from 'react'
import Swal from 'sweetalert2';
import { withRouter, Link } from 'react-router-dom';


class NavBar extends Component {
    onLogOut = () => {
        Swal.fire({
            title: "Sucesso!",
            text: "Logout realizado com sucesso!",
            type: "success",
            button: "Ok!"
          }).then(() => {
            localStorage.removeItem('desafio-keycar');
            this.props.history.push(`/`);
          });
    }
    render() {
        const loginRegLink = (
            <ul>          
              <li><Link to='/'>Chat</Link></li>
              <li><Link to='/login'>Login</Link></li>
              <li><Link to='/cadastro'>Cadastro</Link></li>
            </ul>
          );
        const userLink = (
            <ul>
                <li><Link to='/'>Chat</Link></li>
                <li><Link onClick={this.onLogOut}>Sair</Link></li>
            </ul>
        );
        return (
            <nav className="navbar bg-light">
        <img src={require("../images/logo-keycar.png")} alt='LogoKeyCar' className="logo-nav pointer"/>
        <ul>
          { localStorage.getItem('desafio-keycar') == null ? loginRegLink : userLink }
        </ul>
      </nav>
        )
    }
}

export default withRouter(NavBar);