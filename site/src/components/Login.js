import React, { Component } from "react";
import { Link } from "react-router-dom";
import { login } from "./UserFunctions";
import Swal from "sweetalert2";

export default class Login extends Component {
  constructor() {
    super();
    this.state = {
      nick: "",
      senha: ""
    };
  }

  onSubmit = e => {
    e.preventDefault();
    const usuario = {
      nick: this.state.nick,
      senha: this.state.senha
    };
    login(usuario).then(res => {
      if (res) {
        Swal.fire({
          title: "Sucesso!",
          text: "Login realizado com sucesso!",
          type: "success",
          button: "Ok!"
        }).then(() => {
          this.props.history.push(`/`);
        });
      } else {
        Swal.fire({
          title: "Aviso!",
          text: "Usuário ou senha inválidos!",
          type: "warning",
          button: "Ok!"
        });
      }
    });
  };
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  render() {
    return (
      <div className="wrapper">
        <div className="form-wrapper">
          <h1> Login </h1>{" "}
          <form onSubmit={this.onSubmit} noValidate>
            <div className="firstName">
              <label htmlFor="nick"> Nome </label>{" "}
              <input
                type="text"
                placeholder="Digite seu nome"
                name="nick"
                noValidate
                onChange={this.onChange}
              />{" "}
            </div>{" "}
            <div className="lastName">
              <label htmlFor="senha"> Senha </label>{" "}
              <input
                type="password"
                placeholder="Digite sua senha"
                name="senha"
                noValidate
                onChange={this.onChange}
              />{" "}
            </div>
            <div className="createAccount">
              <button type="submit"> Entrar </button>{" "}
            </div>{" "}
            <div className="createAccount">
              <p>
                <Link to='/cadastro'>Ainda não possui uma conta? Faça seu cadastro.</Link>
              </p>
            </div>
          </form>{" "}
        </div>
      </div>
    );
  }
}
