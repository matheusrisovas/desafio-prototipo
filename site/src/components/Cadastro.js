import React, { Component } from "react";
import { Link } from "react-router-dom";
import Swal from "sweetalert2";
import { registrarUsuario } from "./UserFunctions";

export default class Cadastro extends Component {
  constructor() {
    super();
    this.state = {
      nick: "",
      senha: ""
    };
  }

  onSubmit = e => {
    e.preventDefault();
    const novoUsuario = {
      nick: this.state.nick,
      senha: this.state.senha
    };
    registrarUsuario(novoUsuario).then(res => {
      if (res) {
        Swal.fire({
          title: "Sucesso!",
          text: "Cadastro realizado com sucesso!",
          type: "success",
          button: "Ok!"
        }).then(() => {
          this.props.history.push(`/login`);
        });
      } else {
        Swal.fire({
          title: "Aviso!",
          text: "Informe usuário e senha válidos!",
          type: "warning",
          button: "Ok!"
        });
      }
    });
  };
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  render() {
    return (
      <div className="wrapper">
        <div className="form-wrapper">
          <h1> Cadastro </h1>{" "}
          <form onSubmit={this.onSubmit} noValidate>
            <div className="firstName">
              <label htmlFor="nick"> Nome </label>{" "}
              <input
                type="text"
                placeholder="Digite seu nome"
                name="nick"
                noValidate
                onChange={this.onChange}
              />{" "}
            </div>{" "}
            <div className="lastName">
              <label htmlFor="senha"> Senha </label>{" "}
              <input
                type="password"
                placeholder="Digite sua senha"
                name="senha"
                noValidate
                onChange={this.onChange}
              />{" "}
            </div>
            <div className="createAccount">
              <button type="submit"> Registrar Usuário </button>{" "}
            </div>{" "}
            <div className="createAccount">
              <p>
                <Link to="/login">Já possui uma conta? Faça o seu login.</Link>
              </p>
            </div>
          </form>{" "}
        </div>
      </div>
    );
  }
}
